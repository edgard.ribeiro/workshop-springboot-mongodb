package com.edgardribeiro.workshopmongo.configs;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import com.edgardribeiro.workshopmongo.domain.Post;
import com.edgardribeiro.workshopmongo.domain.User;
import com.edgardribeiro.workshopmongo.dto.AuthorDTO;
import com.edgardribeiro.workshopmongo.dto.CommentDTO;
import com.edgardribeiro.workshopmongo.repositories.PostRepository;
import com.edgardribeiro.workshopmongo.repositories.UserRepository;

@Configuration
public class Instantiation implements CommandLineRunner {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PostRepository postRepository;

	@Override
	public void run(String... args) throws Exception {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		
		userRepository.deleteAll();
		postRepository.deleteAll();
		
		User amanda = new User(null, "Amanda Avelina dos Santos", "amanda.avelina16@gmail.com");
		User sophie = new User(null, "Sophie Avelina Monção Ribeiro", "silviamsophie@gmail.com");
		User edgard = new User(null, "Edgard Monção Ribeiro", "ribeiro.edgard@gmail.com");
		
		userRepository.saveAll(Arrays.asList(amanda, sophie, edgard));
		
		Post post1 = new Post(null, sdf.parse("13/12/2023"), "Partiu viagem", "Vou viajar para São Paulo, Abraços!", new AuthorDTO(amanda));
		Post post2 = new Post(null, sdf.parse("15/12/2023"), "Bom dia", "Acordei feliz hoje", new AuthorDTO(amanda));
		
		CommentDTO c1 = new CommentDTO("Boa viagem esposa!", sdf.parse("13/12/2023"), new AuthorDTO(edgard));
		CommentDTO c2 = new CommentDTO("Aproveite mamãe!", sdf.parse("14/12/2023"), new AuthorDTO(sophie));
		CommentDTO c3 = new CommentDTO("Tenha um ótimo dia minha amada!", sdf.parse("15/12/2023"), new AuthorDTO(edgard));
		
		post1.getComments().addAll(Arrays.asList(c1, c2));
		post2.getComments().addAll(Arrays.asList(c3));
		
		postRepository.saveAll(Arrays.asList(post1, post2));
		
		amanda.getPosts().addAll(Arrays.asList(post1, post2));
		userRepository.save(amanda);
		
		
		
	}

}
